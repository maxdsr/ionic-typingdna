var express        =         require("express");
var bodyParser     =         require("body-parser");
var app            =         express();
var request = require('request');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


const GENERIC_OPTIONS = {
  baseUrl: 'https://api.typingdna.com/',
  auth: {
    user: '79ff46a4320419aafb7b83156f926d04',
    password: '53e5342cd4fb69fe14d44881d4f4d638'
  }
};

app.post('/check-user',function(req,res) {

  const userID = req.body.id;

  request(getCheckUserRequestOptions(userID), (errTDNA, resTDNA, bodyTDNA) => {
    if (errTDNA) {
      console.log(errTDNA);
      return;
    }
    console.log(bodyTDNA);
    res.send(bodyTDNA);
  });
});

app.post('/save-pattern', function (req, res) {

  const userID = req.body.id;
  const typingPattern = req.body.tp;

  request.post(getSavePatternRequestOptions(userID, typingPattern), (errTDNA, resTDNA, bodyTDNA) => {
    if (errTDNA) {
      console.log('Eroarea de la TDNA, reporniti serverul local node.js');
      resTDNA.end(errTDNA);
    }
    else {
      console.log(bodyTDNA);
      res.send(bodyTDNA);
    }
  });
});

app.post('/verify-pattern', function (req, res) {

  const userID = req.body.id;
  const typingPattern = req.body.tp;

  request.post(getVerifyPatternRequestOptions(userID, typingPattern), (errTDNA, resTDNA, bodyTDNA) => {
    if (errTDNA) {
      console.log('Eroarea de la TDNA, reporniti serverul local node.js');
      resTDNA.end(errTDNA);
    }
    else {
      console.log(bodyTDNA);
      res.send(bodyTDNA);
    }
  });
});

app.post('/get-quote', function (req, res) {

  const minLength = req.body.minLength;
  const maxLength = req.body.maxLength;

  request(getQuoteRequestOptions(minLength, maxLength), (errTDNA, resTDNA, bodyTDNA) => {
    if (errTDNA) {
      console.log(errTDNA);
      return;
    }
    console.log(bodyTDNA);
    res.send(bodyTDNA);
  });
});

app.post('/delete-user', function (req, res) {

  const userID = req.body.id;

  request.delete(getDeleteUserRequestOptions(userID), (errTDNA, resTDNA, bodyTDNA) => {
    if (errTDNA) {
      console.log(errTDNA);
      return;
    }
    console.log(bodyTDNA);
    res.send(bodyTDNA);
  });
});

app.listen(3000, function() {
  console.log("Started on PORT 3000");
});

function getCheckUserRequestOptions(userID) {

  const checkUserOptions = GENERIC_OPTIONS;
  checkUserOptions.method = 'GET';
  checkUserOptions.uri = 'user/' + userID;

  return checkUserOptions;
}

function getSavePatternRequestOptions(userID, typingPattern) {
  const savePatternOptions = GENERIC_OPTIONS;
  savePatternOptions.method = 'POST';
  savePatternOptions.uri = 'save/' + userID;
  savePatternOptions.headers = {'content-type' : 'application/x-www-form-urlencoded'};
  savePatternOptions.form = {tp: typingPattern};

  return savePatternOptions;
}

function getVerifyPatternRequestOptions(userID, typingPattern) {
  const verifyPatternOptions = GENERIC_OPTIONS;
  verifyPatternOptions.method = 'POST';
  verifyPatternOptions.uri = 'verify/' + userID;
  verifyPatternOptions.headers = {
    'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
    'cache-control': 'no-cache'
  };
  verifyPatternOptions.formData = {
    tp: typingPattern
  };

  return verifyPatternOptions;
}

function getQuoteRequestOptions(minLength, maxLength) {
  const quoteRequestOptions = GENERIC_OPTIONS;
  quoteRequestOptions.method = 'GET';
  quoteRequestOptions.uri = 'quote';
  quoteRequestOptions.qs = {
    min: minLength,
    max: maxLength
  };

  return quoteRequestOptions;
}

function getDeleteUserRequestOptions(userID) {
  const deleteUserRequestOptions = GENERIC_OPTIONS;
  deleteUserRequestOptions.method = 'DELETE';
  deleteUserRequestOptions.uri = 'user/' + userID;
  deleteUserRequestOptions.headers = {
    'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
    'cache-control': 'no-cache'
  };

  return deleteUserRequestOptions;
}
