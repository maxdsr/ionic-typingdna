import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TypeOneVerificationPage } from './type-one-verification';

@NgModule({
  declarations: [
    TypeOneVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(TypeOneVerificationPage),
  ],
})
export class TypeOneVerificationPageModule {}
