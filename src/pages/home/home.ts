import {Component, ViewChild} from '@angular/core';
import { NavController } from 'ionic-angular';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
              private httpClient: HttpClient) {
  }

}
