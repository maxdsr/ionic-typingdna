import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpHeaders} from "@angular/common/http";

/**
 * Generated class for the TypeOneRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare const TypingDNA;

@IonicPage()
@Component({
  selector: 'page-type-one-register',
  templateUrl: 'type-one-register.html',
})
export class TypeOneRegisterPage {

  textToCopy: string;
  apiResponse: Object;
  enrollmentForm: FormGroup;
  typingDNAinstance: any;
  @ViewChild('enrollmentText') ionTextAreaView: any;

  constructor(public navCtrl: NavController,
              private httpClient: HttpClient,
              private formBuilder: FormBuilder) {

    this.textToCopy = 'myEmail@email.md';
    this.apiResponse = "Not yet..";
    this.typingDNAinstance = new TypingDNA();

    this.enrollmentForm = this.formBuilder.group({
      userID : ['', Validators.required],
      enrollmentText : ['', Validators.required]
    });
  }

  ionViewDidLoad() {

    this.ionTextAreaView._elementRef.nativeElement.children[0].id = 'enrollmentTextInput';
    this.typingDNAinstance.addTarget('enrollmentTextInput');

    setInterval(() => {
      console.log(this.typingDNAinstance.getTypingPattern({type: 1, length: this.textToCopy.length}));
    }, 3000);

  }

  submitEnrollmentForm(form: any) {

    this.typingDNAinstance.stop();

    console.log(form.userID);

    const textLength = this.textToCopy.length;

    this.makeEnrollmentAPIrequest(form.userID, this.typingDNAinstance.getTypingPattern({type: 0, length: textLength}));
  }

  makeEnrollmentAPIrequest(userID: string, typingPattern: string) {
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    const body = new URLSearchParams();
    body.set('id', userID);
    body.set('tp', typingPattern);

    this.httpClient.post('http://localhost:3000/save-pattern', body.toString(), options)
      .subscribe((response: any) => {
        console.log('Asta-i API Response : ', response);
        this.apiResponse = response;
        this.typingDNAinstance.reset();
      });
  }

}
