import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TypeOneRegisterPage } from './type-one-register';

@NgModule({
  declarations: [
    TypeOneRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(TypeOneRegisterPage),
  ],
})
export class TypeOneRegisterPageModule {}
