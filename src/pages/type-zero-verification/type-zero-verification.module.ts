import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TypeZeroVerificationPage } from './type-zero-verification';

@NgModule({
  declarations: [
    TypeZeroVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(TypeZeroVerificationPage),
  ],
})
export class TypeZeroVerificationPageModule {}
