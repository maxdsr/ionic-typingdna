import {Component, Inject, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient, HttpHeaders} from "@angular/common/http";

declare const TypingDNA;

@IonicPage()
@Component({
  selector: 'page-type-zero-verification',
  templateUrl: 'type-zero-verification.html',
})
export class TypeZeroVerificationPage {

  textToCopy: string;
  apiResponse: Object;
  verificationForm: FormGroup;
  typingDNAinstance: any;
  setIntervalinstance: any;
  readonly HTML_INPUT_ID: string;
  @ViewChild('verificationTextArea') ionTextAreaView: any;

  constructor(public navCtrl: NavController,
              private httpClient: HttpClient,
              private formBuilder: FormBuilder,
              @Inject('typingDNAInstance') typingDNAinstance12: any) {

    this.HTML_INPUT_ID = 'verificationTextInput';
    this.textToCopy = "Please wait...";
    this.apiResponse = "Not yet..";
    this.typingDNAinstance = typingDNAinstance12;

    this.verificationForm = this.formBuilder.group({
      userID : ['', Validators.required],
      verificationText : ['', Validators.required]
    });
  }

  ionViewDidLoad() {

    this.ionTextAreaView._elementRef.nativeElement.children[0].id = this.HTML_INPUT_ID;
    this.typingDNAinstance.addTarget(this.HTML_INPUT_ID);

    this.setIntervalinstance = setInterval(() => {
      console.log(this.typingDNAinstance.getTypingPattern({type: 0, length: 131}));
    }, 3000);

    this.getVerificationText();
  }

  ionViewWillLeave() {

    clearInterval(this.setIntervalinstance);

    this.ionTextAreaView._elementRef.nativeElement.children[0].removeAttribute('id');
    this.typingDNAinstance.removeTarget(this.HTML_INPUT_ID);

    this.typingDNAinstance.reset();
    this.typingDNAinstance.stop();
  }

  getVerificationText() {

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    const body = new URLSearchParams();
    body.set('minLength', '130');
    body.set('maxLength', '170');

    this.httpClient.post('http://localhost:3000/get-quote', body.toString(), options)
      .subscribe((response: any) => {
        console.log(response);
        this.textToCopy = response.quote;
      });
  }

  submitVerificationForm(form: any) {

    this.typingDNAinstance.stop();

    const textLength = this.textToCopy.length;
    this.makeVerificationAPIrequest(form.userID, this.typingDNAinstance.getTypingPattern({type: 0, length: textLength}));
  }

  makeVerificationAPIrequest(userID: string, typingPattern: string) {
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    const body = new URLSearchParams();
    body.set('id', userID);
    body.set('tp', typingPattern);


    this.apiResponse = "Please wait, request is processing...";
    this.httpClient.post('http://localhost:3000/verify-pattern', body.toString(), options)
      .subscribe((response: any) => {
        console.log('Asta-i API Response : ', response);
        this.apiResponse = response;
        this.typingDNAinstance.reset();
      },
      (error: any) => {
        this.apiResponse = "Something went wrong...";
      });
  }

}
