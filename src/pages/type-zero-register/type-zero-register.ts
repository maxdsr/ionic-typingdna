import {Component, Inject, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

/**
 * Generated class for the TypeZeroRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// declare const TypingDNA;

@IonicPage()
@Component({
  selector: 'page-type-zero-register',
  templateUrl: 'type-zero-register.html',
})
export class TypeZeroRegisterPage {

  textToCopy: string;
  apiResponse: Object;
  enrollmentForm: FormGroup;
  typingDNAinstance12: any;
  @ViewChild('enrollmentTextArea') ionTextAreaView: any;

  constructor(public navCtrl: NavController,
              private httpClient: HttpClient,
              private formBuilder: FormBuilder,
              @Inject('typingDNAInstance') typingDNAinstance12: any) {

    this.textToCopy = "Please wait...";
    this.apiResponse = "Not yet..";
    // this.typingDNAinstance12 = new TypingDNA();
    this.typingDNAinstance12 = typingDNAinstance12;

    this.enrollmentForm = this.formBuilder.group({
      userID : ['', Validators.required],
      enrollmentText : ['', Validators.required]
    });
  }

  ionViewDidLoad() {

    this.ionTextAreaView._elementRef.nativeElement.children[0].id = 'enrollmentTextInput1';
    this.typingDNAinstance12.addTarget('enrollmentTextInput1');
    this.typingDNAinstance12.start();

    setInterval(() => {
      console.log(this.typingDNAinstance12.getTypingPattern({type: 0, length: 131}));
    }, 3000);


    this.getVerificationText();

  }


  getVerificationText() {

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    const body = new URLSearchParams();
    body.set('minLength', '130');
    body.set('maxLength', '170');

    this.httpClient.post('http://localhost:3000/get-quote', body.toString(), options)
      .subscribe((response: any) => {
        console.log(response);
        this.textToCopy = response.quote;
      });
  }

  submitEnrollmentForm(form: any) {

    this.typingDNAinstance12.stop();

    console.log(form.userID);

    const textLength = this.textToCopy.length;

    this.makeEnrollmentAPIrequest(form.userID, this.typingDNAinstance12.getTypingPattern({type: 0, length: textLength}));
  }

  makeEnrollmentAPIrequest(userID: string, typingPattern: string) {
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    const body = new URLSearchParams();
    body.set('id', userID);
    body.set('tp', typingPattern);

    this.httpClient.post('http://localhost:3000/save-pattern', body.toString(), options)
      .subscribe((response: any) => {
        console.log('Asta-i API Response : ', response);
        this.apiResponse = response;
        this.typingDNAinstance12.reset();
      });
  }

  onRemoveTarget() {
    this.typingDNAinstance12.reset();
    this.typingDNAinstance12.removeTarget('enrollmentTextInput1');
  }

  onAddTarget() {
    this.typingDNAinstance12.addTarget('enrollmentTextInput1');
  }

}
