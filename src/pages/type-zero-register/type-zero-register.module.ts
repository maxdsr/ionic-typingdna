import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TypeZeroRegisterPage } from './type-zero-register';

@NgModule({
  declarations: [
    TypeZeroRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(TypeZeroRegisterPage),
  ],
})
export class TypeZeroRegisterPageModule {}
