import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TypeZeroRegisterPage} from "../pages/type-zero-register/type-zero-register";
import {TypeOneRegisterPage} from "../pages/type-one-register/type-one-register";
import {ReactiveFormsModule} from "@angular/forms";
import {TypeZeroVerificationPage} from "../pages/type-zero-verification/type-zero-verification";
import {TypeOneVerificationPage} from "../pages/type-one-verification/type-one-verification";

declare const TypingDNA;

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    TypeZeroRegisterPage,
    TypeOneRegisterPage,
    TypeZeroVerificationPage,
    TypeOneVerificationPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    TypeZeroRegisterPage,
    TypeOneRegisterPage,
    TypeZeroVerificationPage,
    TypeOneVerificationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpClient,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: 'typingDNAInstance', useValue: new TypingDNA()}
  ]
})
export class AppModule {}
