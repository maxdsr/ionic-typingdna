import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import {TypeZeroRegisterPage} from "../pages/type-zero-register/type-zero-register";
import {TypeOneRegisterPage} from "../pages/type-one-register/type-one-register";
import {TypeZeroVerificationPage} from "../pages/type-zero-verification/type-zero-verification";
import {TypeOneVerificationPage} from "../pages/type-one-verification/type-one-verification";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = TypeZeroRegisterPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage },
      { title: 'Type 0 Register', component: TypeZeroRegisterPage },
      { title: 'Type 0 Verification', component: TypeZeroVerificationPage },
      { title: 'Type 1 Register', component: TypeOneRegisterPage },
      { title: 'Type 1 Verification', component: TypeOneVerificationPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
